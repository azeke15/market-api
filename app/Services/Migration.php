<?php

namespace App\Services;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Migration
{
    public $table_name;

    public function __construct($table_name)
    {
        $this->table_name = $table_name;
    }

    public function createTable()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->id();
            $table->string('title')->index($this->table_name . '_title');
            $table->string('image_path');
            $table->boolean('is_published')->index($this->table_name . '_is_published');
        });
    }

    public function renameTable($new_table_name)
    {
        Schema::rename($this->table_name, $new_table_name);
    }

    public function dropTable()
    {
        Schema::dropIfExists($this->table_name);
    }

    public function createColumnInTable($column_name)
    {
        Schema::table($this->table_name, function (Blueprint $table) use ($column_name) {
            $table->string($column_name)->index($this->table_name . '_' . $column_name)->default('');
        });
    }

    public function renameColumnInTable($old_column_name, $new_column_name)
    {
        Schema::table($this->table_name, function (Blueprint $table) use ($old_column_name, $new_column_name) {
            $table->renameColumn($old_column_name, $new_column_name);
        });
    }

    public function dropColumnInTable($column_name)
    {
        Schema::table($this->table_name, function (Blueprint $table) use ($column_name) {
            $table->dropColumn($column_name);
        });
    }
}