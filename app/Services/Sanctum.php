<?php

namespace App\Services;

use App\User;

class Sanctum
{
    private $user;
    private $device;

    public function __construct(User $user, $device)
    {
        $this->user = $user;
        $this->device = $device;
    }

    private function deleteUserTokensByDevice()
    {
        $this->user->tokens()->where('name', $this->device)->delete();
    }

    public function getUserGeneratedToken()
    {
        $this->deleteUserTokensByDevice();

        return $this->user->createToken($this->device)->plainTextToken;
    }
}