<?php

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Redis;

class MessageGenerator
{
    public static function getMessageForUserActivation(User $user)
    {
        $code = CodeGenerator::getGeneratedCode();
        $message = 'soa.kz Код активации: ' . $code;
        Redis::set('user:' . $user->id . ':code', $code);

        return $message;
    }
}