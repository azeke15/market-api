<?php

namespace App\Services;

class CodeGenerator
{
    public static function getGeneratedCode()
    {
        return rand(1000, 9999);
    }
}
