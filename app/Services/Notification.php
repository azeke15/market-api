<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Notification
{
    private $phone;
    private $message;

    public function __construct($phone, $message)
    {
        $this->phone = $phone;
        $this->message = $message;
    }

    public function sendNotification()
    {
        if (env('APP_DEPLOY')) {
            $login = env('SMSC_LOGIN');
            $password = env('SMSC_PASSWORD');
            Http::get('https://smsc.kz/sys/send.php?'
                . 'login=' . $login
                . '&psw=' . $password
                . '&phones=' . $this->phone
                . '&mes=' . $this->message
            );
        } else {
            Log::info($this->message);
        }
    }
}