<?php

namespace App\Http\Requests\FilterList;

use App\FilterList;
use Illuminate\Foundation\Http\FormRequest;

class FilterListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter_id' => ['required', 'integer', 'exists:App\Filter,id'],
            'title' => ['required', 'string']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $filter_id = $this->request->get('filter_id');
            $title = $this->request->get('title');
            if (FilterList::getFilterListByFilterIdAndTitle($filter_id, $title)) abort(422, 'Filter list already exists');
        });
    }
}
