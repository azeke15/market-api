<?php

namespace App\Http\Requests\FilterList;

use App\FilterList;
use Illuminate\Foundation\Http\FormRequest;

class FilterListUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $exploded_uri = explode('/', request()->getUri());
            $filterlist_id = $exploded_uri[count($exploded_uri) - 1];
            $filterlist = FilterList::find($filterlist_id);
            $title = $this->request->get('title');
            if (FilterList::getFilterListByFilterIdAndTitle($filterlist->filter_id, $title)) abort(422, 'Filter list already exists');
        });
    }
}
