<?php

namespace App\Http\Requests\Product;

use App\Product;
use App\Services\Migration;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class ProductDestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $exploded_uri = explode('/', request()->getUri());
            $product_id = $exploded_uri[count($exploded_uri) - 1];
            $product = Product::find($product_id);
            if (!$product) abort(404, 'Not found');
            Storage::deleteDirectory('public/product/' . $product->alias);


            $migration = new Migration($product->alias);
            $migration->dropTable();
        });
    }
}
