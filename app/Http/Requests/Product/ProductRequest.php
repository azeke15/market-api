<?php

namespace App\Http\Requests\Product;

use App\Services\Migration;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'subcategory_id' => ['required', 'integer', 'exists:App\Subcategory,id'],
            'image' => ['required', 'image'],
            'is_published' => ['boolean']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $alias = Str::slug($this->request->get('title'));
            $image_path = request()->file('image')->store('public/product/' . $alias);
            $this->request->set('image_path', $image_path);
            $this->request->set('alias', $alias);

            $migration = new Migration($alias);
            $migration->createTable();
        });
    }
}
