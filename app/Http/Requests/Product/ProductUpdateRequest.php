<?php

namespace App\Http\Requests\Product;

use App\Product;
use App\Services\Migration;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'image' => ['image'],
            'is_published' => ['required', 'boolean']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $exploded_uri = explode('/', request()->getUri());
            $product_id = $exploded_uri[count($exploded_uri) - 1];
            $product = Product::find($product_id);
            if (!$product) abort(404, 'Not found');
            $alias = Str::slug($this->request->get('title'));
            $is_title_changed = $product->title !== $this->request->get('title');
            if (request()->file('image')) {
                Storage::deleteDirectory('public/product/' . $product->alias);
                $image_path = request()->file('image')->store('public/product/' . $alias);
            } else {
                if ($is_title_changed) {
                    Storage::move('public/product/' . $product->alias, 'public/product/' . $alias);
                    $image_path = str_replace($product->alias, $alias, $product->image_path);

                    $migration = new Migration($product->alias);
                    $migration->renameTable($alias);
                } else {
                    $image_path = $product->image_path;
                }
            }

            $this->request->set('image_path', $image_path);
            $this->request->set('alias', $alias);
        });
    }
}
