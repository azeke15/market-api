<?php

namespace App\Http\Requests\City;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'unique:App\City,title']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $this->request->set('alias', Str::slug($this->request->get('title')));
        });
    }
}
