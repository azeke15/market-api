<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'image' => ['image']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $user = $this->user();
            $first_name = $this->request->get('first_name');
            $last_name = $this->request->get('last_name');
            $avatar_path = $user->avatar_path;
            $fio_is_changed = $first_name !== $user->first_name || $last_name !== $user->last_name;
            if ($user->passport && $fio_is_changed) abort(422, 'Impossible to change FIO after passport moderation');

            if (request()->file('image')) {
                if ($user->avatar_path) {
                    Storage::delete($user->avatar_path);
                }

                $avatar_path = $image_path = request()->file('image')->store('public/user/' . $user->id);
            }

            $this->request->set('password', md5($this->request->get('password')));
            $this->request->set('avatar_path', $avatar_path);
            $this->request->remove('image');
        });
    }
}
