<?php

namespace App\Http\Requests\User;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => ['required', 'integer'],
            'password' => ['required', 'string'],
            'device' => ['required', 'string']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $user = User::getUserByPhone($this->request->get('phone'));

            if (!$user || $user->password != md5($this->request->get('password'))) {
                abort(404, 'User not found');
            }

            if (!$user->is_activated) {
                abort(422, 'User is not activated');
            }
        });
    }
}
