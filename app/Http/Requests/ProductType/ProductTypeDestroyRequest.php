<?php

namespace App\Http\Requests\ProductType;

use App\ProductType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class ProductTypeDestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $product = request('product_type');
            if (!$product) abort(422, 'There is no product_type parameter');

            $exploded_uri = explode('/', request()->getUri());
            $exploded_uri_last_cell = explode('?', $exploded_uri[count($exploded_uri) - 1]);
            $product_type_id = $exploded_uri_last_cell[0];
            $product_type = ProductType::findProductTypeByProductAndId($product, $product_type_id);
            Storage::delete($product_type['image_path']);
        });
    }
}
