<?php

namespace App\Http\Requests\ProductType;

use App\ProductType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class ProductTypeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'image' => ['image'],
            'is_published' => ['required', 'boolean']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $product = request('product_type');
            if (!$product) abort(422, 'There is no product_type parameter');

            $exploded_uri = explode('/', request()->getUri());
            $exploded_uri_last_cell = explode('?', $exploded_uri[count($exploded_uri) - 1]);
            $product_type_id = $exploded_uri_last_cell[0];
            $product_type = ProductType::findProductTypeByProductAndId($product, $product_type_id);
            if (!$product_type) abort(404, 'Product type not found');

            if (request()->file('image')) {
                Storage::delete($product_type['image_path']);
                $image_path = request()->file('image')->store('public/product/' . $product);
            } else {
                $image_path = $product_type['image_path'];
            }

            if ($this->request->get('title') !== $product_type['title']) {
                $existing_product_type = ProductType::getProductTypeByProductAndTitle($product, $this->request->get('title'));
                if ($existing_product_type) abort(422, 'Product type title already exists');
            }

            $this->request->set('image_path', $image_path);
        });
    }
}
