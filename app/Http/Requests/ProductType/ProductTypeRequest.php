<?php

namespace App\Http\Requests\ProductType;

use App\ProductType;
use Illuminate\Foundation\Http\FormRequest;

class ProductTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'image' => ['required', 'image'],
            'is_published' => ['required', 'boolean']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $product = request('product_type');
            $product_type = ProductType::getProductTypeByProductAndTitle($product, $this->request->get('title'));
            if ($product_type) abort(422, 'Product type title already exists');

            $image_path = request()->file('image')->store('public/product/' . $product);
            $this->request->set('image_path', $image_path);
        });
    }
}
