<?php

namespace App\Http\Requests\Filter;

use App\Filter;
use App\Services\Migration;
use Illuminate\Foundation\Http\FormRequest;

class FilterDestroyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $exploded_uri = explode('/', request()->getUri());
            $filter_id = $exploded_uri[count($exploded_uri) - 1];
            $filter = Filter::find($filter_id);

            $migration = new Migration($filter->product->alias);
            $migration->dropColumnInTable($filter->alias);
        });
    }
}
