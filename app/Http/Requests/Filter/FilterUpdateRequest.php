<?php

namespace App\Http\Requests\Filter;

use App\Filter;
use App\Services\Migration;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class FilterUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $exploded_uri = explode('/', request()->getUri());
            $filter_id = $exploded_uri[count($exploded_uri) - 1];
            $filter = Filter::find($filter_id);
            $alias = Str::slug($this->request->get('title'));
            $this->request->set('alias', $alias);

            $migration = new Migration($filter->product->alias);
            $migration->renameColumnInTable($filter->alias, $alias);
        });
    }
}
