<?php

namespace App\Http\Requests\Filter;

use App\Filter;
use App\Product;
use App\Services\Migration;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required', 'integer', 'exists:App\Product,id'],
            'title' => ['required', 'string']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $product_id = $this->request->get('product_id');
            $title = $this->request->get('title');
            if (Filter::getFilterByProductIdAndTitle($product_id, $title)) abort(422, 'Filter already exists');
            $alias = Str::slug($title);
            $product = Product::find($product_id);
            $this->request->set('alias', $alias);

            $migration = new Migration($product->alias);
            $migration->createColumnInTable($alias);
        });
    }
}
