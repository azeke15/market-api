<?php

namespace App\Http\Requests\Passport;

use Illuminate\Foundation\Http\FormRequest;

class PassportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'passport_id' => ['required', 'integer', 'digits:12', 'unique:App\Passport,passport_id'],
            'passport_front' => ['required', 'image'],
            'passport_back' => ['required', 'image']
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $this->request->set('user_id', request()->user()->id);
            $passport_front_path = request()->file('passport_front')->store('public/passports/' . request()->user()->id);
            $passport_back_path = request()->file('passport_back')->store('public/passports/' . request()->user()->id);
            $this->request->set('passport_front_path', $passport_front_path);
            $this->request->set('passport_back_path', $passport_back_path);
        });
    }
}
