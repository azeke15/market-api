<?php

namespace App\Http\Controllers;

use App\Filter;
use App\Http\Requests\Filter\FilterDestroyRequest;
use App\Http\Requests\Filter\FilterRequest;
use App\Http\Requests\Filter\FilterUpdateRequest;
use App\Http\Resources\Filter\FilterResource;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function index(Request $request)
    {
        $filters = Filter::getFiltersByProductId($request->product_id);

        return FilterResource::collection($filters);
    }

    public function store(FilterRequest $filterRequest)
    {
        $filter = Filter::create($filterRequest->toArray());

        return new FilterResource($filter);
    }

    public function show(int $id)
    {
        $filter = Filter::find($id);

        return new FilterResource($filter);
    }

    public function update(FilterUpdateRequest $filterUpdateRequest, int $id)
    {
        $filter = Filter::find($id);
        $filter->title = $filterUpdateRequest->title;
        $filter->alias = $filterUpdateRequest->alias;
        $filter->save();

        return new FilterResource($filter);
    }

    public function destroy(FilterDestroyRequest $filterDestroyRequest, int $id)
    {
        Filter::destroy($id);

        return response()->json(['result' => 'success']);
    }
}
