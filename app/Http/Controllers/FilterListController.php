<?php

namespace App\Http\Controllers;

use App\FilterList;
use App\Http\Requests\FilterList\FilterListRequest;
use App\Http\Requests\FilterList\FilterListUpdateRequest;
use App\Http\Resources\FilterList\FilterListResource;
use Illuminate\Http\Request;

class FilterListController extends Controller
{
    public function index(Request $request)
    {
        $filterlists = FilterList::getFilterListsByFilterId($request->filter_id);

        return FilterListResource::collection($filterlists);
    }

    public function store(FilterListRequest $filterListRequest)
    {
        $filterlist = FilterList::create($filterListRequest->toArray());

        return new FilterListResource($filterlist);
    }

    public function show(int $id)
    {
        $filterlist = FilterList::find($id);

        return new FilterListResource($filterlist);
    }

    public function update(FilterListUpdateRequest $filterListUpdateRequest, int $id)
    {
        $filterlist = FilterList::find($id);
        $filterlist->title = $filterListUpdateRequest->title;
        $filterlist->save();

        return new FilterListResource($filterlist);
    }

    public function destroy(int $id)
    {
        FilterList::destroy($id);

        return response()->json(['result' => 'success']);
    }
}
