<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductDestroyRequest;
use App\Http\Requests\Product\ProductRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Http\Resources\Product\ProductResource;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::getProductsBySubcategoryId($request->subcategory_id);

        return ProductResource::collection($products);
    }

    public function store(ProductRequest $productRequest)
    {
        $product = Product::create($productRequest->toArray());

        return new ProductResource($product);
    }

    public function show(int $id)
    {
        $product = Product::find($id);

        return new ProductResource($product);
    }

    public function update(ProductUpdateRequest $productUpdateRequest, int $id)
    {
        $product = Product::find($id);
        $product->title = $productUpdateRequest->title;
        $product->alias = $productUpdateRequest->alias;
        $product->image_path = $productUpdateRequest->image_path;
        $product->is_published = $productUpdateRequest->is_published;
        $product->save();

        return new ProductResource($product);
    }

    public function destroy(ProductDestroyRequest $productDestroyRequest, int $id)
    {
        Product::destroy($id);

        return response()->json(['result' => 'success']);
    }
}
