<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserActivateRequest;
use App\Http\Requests\User\UserLoginRequest;
use App\Http\Requests\User\UserRegisterRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Resources\User\UserResource;
use App\Jobs\SendNotificationJob;
use App\Services\MessageGenerator;
use App\Services\Sanctum;
use App\User;

class UserController extends Controller
{
    public function register(UserRegisterRequest $registerRequest)
    {
        $user = User::create($registerRequest->toArray());
        $message = MessageGenerator::getMessageForUserActivation($user);
        dispatch(new SendNotificationJob($user->phone, $message))->onQueue('send_notification');

        return new UserResource($user);
    }

    public function activate(UserActivateRequest $activateRequest)
    {
        $user = User::getUserByPhone($activateRequest->phone);
        User::activateUserStatus($user, $activateRequest->code);

        return new UserResource($user);
    }

    public function login(UserLoginRequest $loginRequest)
    {
        $user = User::getUserByPhone($loginRequest->phone);
        $sanctum = new Sanctum($user, $loginRequest->device);
        $user->token = $sanctum->getUserGeneratedToken();

        return new UserResource($user);
    }

    public function update(UserUpdateRequest $userUpdateRequest)
    {
        $user = $userUpdateRequest->user();
        $user->password = $userUpdateRequest->password;
        $user->first_name = $userUpdateRequest->first_name;
        $user->last_name = $userUpdateRequest->last_name;
        $user->avatar_path = $userUpdateRequest->avatar_path;
        $user->save();

        return new UserResource($user);
    }
}
