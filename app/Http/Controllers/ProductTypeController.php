<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductType\ProductTypeDestroyRequest;
use App\Http\Requests\ProductType\ProductTypeRequest;
use App\Http\Requests\ProductType\ProductTypeUpdateRequest;
use App\Http\Resources\ProductType\ProductTypeResource;
use App\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    public function index(Request $request)
    {
        $product_types = ProductType::getAllByProduct($request->product_type);

        return ProductTypeResource::collection($product_types);
    }

    public function store(ProductTypeRequest $productTypeRequest)
    {
        $product_type = ProductType::create($productTypeRequest);

        return new ProductTypeResource($product_type);
    }

    public function show(Request $request, int $id)
    {
        $product_type = ProductType::findProductTypeByProductAndId($request->product_type, $id);

        return new ProductTypeResource($product_type);
    }

    public function update(ProductTypeUpdateRequest $productTypeUpdateRequest, int $id)
    {
        $product_type = ProductType::updateProductTypeByProductAndId($productTypeUpdateRequest->except('image', '_method', 'product_type'), $id);

        return new ProductTypeResource($product_type);
    }

    public function destroy(ProductTypeDestroyRequest $productTypeDestroyRequest, int $id)
    {
        ProductType::deleteProductTypeByProductAndId($productTypeDestroyRequest->product_type, $id);

        return response()->json(['result' => 'success']);
    }
}
