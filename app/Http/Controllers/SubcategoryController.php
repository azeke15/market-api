<?php

namespace App\Http\Controllers;

use App\Http\Requests\Subcategory\SubcategoryRequest;
use App\Http\Requests\Subcategory\SubcategoryUpdateRequest;
use App\Http\Resources\Subcategory\SubcategoryResource;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SubcategoryController extends Controller
{
    public function index(Request $request)
    {
        $subcategories = Subcategory::getSubcategoriesByCategoryId($request->category_id);

        return SubcategoryResource::collection($subcategories);
    }

    public function store(SubcategoryRequest $subcategoryRequest)
    {
        $subcategory = Subcategory::create($subcategoryRequest->toArray());

        return new SubcategoryResource($subcategory);
    }

    public function show(int $id)
    {
        $subcategory = Subcategory::find($id);

        return new SubcategoryResource($subcategory);
    }

    public function update(SubcategoryUpdateRequest $subcategoryRequest, int $id)
    {
        $subcategory = Subcategory::find($id);
        $subcategory->title = $subcategoryRequest->title;
        $subcategory->alias = $subcategoryRequest->alias;
        $subcategory->save();

        return new SubcategoryResource($subcategory);
    }

    public function destroy(int $id)
    {
        Subcategory::destroy($id);

        return response()->json(['result' => 'success']);
    }
}
