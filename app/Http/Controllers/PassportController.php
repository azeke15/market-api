<?php

namespace App\Http\Controllers;

use App\Http\Requests\Passport\PassportRequest;
use App\Http\Resources\User\UserResource;
use App\Passport;
use App\User;
use Illuminate\Http\Request;

class PassportController extends Controller
{
    public function store(PassportRequest $passportRequest)
    {
        Passport::create($passportRequest->toArray());
        $user = User::find($passportRequest->user_id);

        return new UserResource($user);
    }

    public function update(Request $request)
    {
        $user_id = $request->user()->id;
        $passport = Passport::getPassportByUserId($user_id);
        $passport->is_moderated = 1;
        $passport->save();

        return new UserResource(User::find($user_id));
    }
}
