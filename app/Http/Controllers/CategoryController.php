<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Category\CategoryRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Subcategory;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return CategoryResource::collection($categories);
    }

    public function store(CategoryRequest $categoryRequest)
    {
        $category = Category::create($categoryRequest->toArray());

        return new CategoryResource($category);
    }

    public function show(int $id)
    {
        $category = Category::find($id);

        return new CategoryResource($category);
    }

    public function update(CategoryRequest $categoryRequest, int $id)
    {
        $category = Category::find($id);
        $category->title = $categoryRequest->title;
        $category->alias = $categoryRequest->alias;
        $category->save();

        return new CategoryResource($category);
    }

    public function destroy(int $id)
    {
        Category::destroy($id);

        return response()->json(['result' => 'success']);
    }
}
