<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\City\CityRequest;
use App\Http\Resources\City\CityResource;
use Illuminate\Support\Str;

class CityController extends Controller
{
    public function index()
    {
        $cities = City::all();

        return CityResource::collection($cities);
    }

    public function store(CityRequest $cityRequest)
    {
        $city = City::create($cityRequest->toArray());

        return new CityResource($city);
    }

    public function show(int $id)
    {
        $city = City::find($id);

        return new CityResource($city);
    }

    public function update(CityRequest $cityRequest, int $id)
    {
        $city = City::find($id);
        $city->title = $cityRequest->title;
        $city->alias = $cityRequest->alias;
        $city->save();

        return new CityResource($city);
    }

    public function destroy(int $id)
    {
        City::destroy($id);

        return response()->json(['result' => 'success']);
    }
}
