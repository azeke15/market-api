<?php

namespace App\Http\Resources\Filter;

use App\Http\Resources\FilterList\FilterListResource;
use App\Http\Resources\Product\ProductSingleResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'alias' => $this->alias,
            'product' => new ProductSingleResource($this->product),
            'filterlists' => FilterListResource::collection($this->filterlists)
        ];
    }
}
