<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Subcategory\SubcategorySingleResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ProductSingleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'alias' => $this->alias,
            'is_published' => $this->is_published,
            'image_path' => Storage::url($this->image_path),
            'subcategory' => new SubcategorySingleResource($this->subcategory)
        ];
    }
}
