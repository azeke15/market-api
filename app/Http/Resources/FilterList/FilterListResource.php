<?php

namespace App\Http\Resources\FilterList;

use App\Http\Resources\Filter\FilterSingleResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FilterListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'filter' => new FilterSingleResource($this->filter)
        ];
    }
}
