<?php

namespace App\Http\Resources\Subcategory;

use App\Category;
use App\Http\Resources\Category\CategorySingleResource;
use App\Http\Resources\Product\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SubcategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'alias' => $this->alias,
            'category' => new CategorySingleResource($this->category),
            'products' => ProductResource::collection($this->products)
        ];
    }
}
