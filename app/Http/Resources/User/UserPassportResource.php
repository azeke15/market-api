<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserPassportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'passport_id' => $this->passport_id,
            'is_moderated' => $this->is_moderated,
            'passport_front_path' => $this->passport_front_path,
            'passport_back_path' => $this->passport_back_path
        ];
    }
}
