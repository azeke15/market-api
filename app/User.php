<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Redis;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'phone', 'password', 'first_name', 'last_name', 'avatar_path', 'is_activated'
    ];

    protected $hidden = [
        'password'
    ];

    public function passport()
    {
        return $this->hasOne('App\Passport');
    }

    public static function getUserByPhone($phone)
    {
        return self::where('phone', $phone)->first();
    }

    public static function activateUserStatus(self $user, $code)
    {
        $user_code = Redis::get('user:' . $user->id . ':code');

        if ($user_code === $code) {
            $user->is_activated = 1;
            $user->save();
        } else {
            abort(422, 'Invalid code');
        }
    }
}
