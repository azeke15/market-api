<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = ['title', 'alias', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public static function getSubcategoriesByCategoryId($category_id)
    {
        return self::where('category_id', $category_id)->get();
    }

    public static function deleteSubcategoriesByCategoryId($category_id)
    {
        self::where('category_id', $category_id)->delete();
    }
}
