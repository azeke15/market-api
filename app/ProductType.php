<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductType extends Model
{
    public static function getProductTypeByProductAndTitle($product, $title)
    {
        return DB::table($product)->where('title', $title)->first();
    }

    public static function findProductTypeByProductAndId($product, $id)
    {
        return (array) DB::table($product)->find($id);
    }

    public static function create($request)
    {
        $product_type_params = $request->except('product_type', 'image');
        $product_type_id = DB::table($request->product_type)->insertGetId($product_type_params);

        return array_merge(['id' => $product_type_id], $product_type_params);
    }

    public static function getAllByProduct($product)
    {
        $product_types_obj = DB::table($product)->get()->toArray();

        return array_map(function ($value) {
            return (array) $value;
        }, $product_types_obj);
    }

    public static function updateProductTypeByProductAndId($request, $id)
    {
        DB::table(request('product_type'))
            ->where('id', $id)
            ->update($request);

        return self::findProductTypeByProductAndId(request('product_type'), $id);
    }

    public static function deleteProductTypeByProductAndId($product, $id)
    {
        DB::table($product)->where('id', $id)->delete();
    }
}
