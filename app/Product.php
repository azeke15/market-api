<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title', 'alias', 'subcategory_id', 'image_path', 'is_published'];

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function filters()
    {
        return $this->hasMany('App\Filter');
    }

    public static function getProductsBySubcategoryId($subcategory_id)
    {
        return self::where('subcategory_id', $subcategory_id)->get();
    }

    public static function deleteProductsBySubcategoryId($subcategory_id)
    {
        return self::where('subcategory_id', $subcategory_id)->delete();
    }
}
