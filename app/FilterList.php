<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilterList extends Model
{
    protected $fillable = ['filter_id', 'title'];
    protected $table = 'filterlists';

    public function filter()
    {
        return $this->belongsTo('App\Filter');
    }

    public static function getFilterListsByFilterId($filter_id)
    {
        return self::where('filter_id', $filter_id)->get();
    }

    public static function getFilterListByFilterIdAndTitle($filter_id, $title)
    {
        return self::where('filter_id', $filter_id)->where('title', $title)->first();
    }

    public static function deleteFilterListsByFilterId($filter_id)
    {
        self::where('filter_id', $filter_id)->delete();
    }
}
