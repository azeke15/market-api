<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Passport extends Model
{
    protected $fillable = ['user_id', 'passport_id', 'passport_front_path', 'passport_back_path', 'is_moderated'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function getPassportByUserId($user_id)
    {
        return self::where('user_id', $user_id)->first();
    }
}
