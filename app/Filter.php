<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = ['product_id', 'title', 'alias'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function filterlists()
    {
        return $this->hasMany('App\FilterList');
    }

    public static function getFiltersByProductId($product_id)
    {
        return self::where('product_id', $product_id)->get();
    }

    public static function getFilterByProductIdAndTitle($product_id, $title)
    {
        return self::where('product_id', $product_id)->where('title', $title)->first();
    }

    public static function deleteFiltersByProductId($product_id)
    {
        self::where('product_id', $product_id)->delete();
    }
}
