<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('passport', 'PassportController')->only(['store', 'update']);
    Route::apiResource('category', 'CategoryController')->except(['create', 'edit']);
    Route::apiResource('subcategory', 'SubcategoryController')->except(['create', 'edit']);
    Route::apiResource('city', 'CityController')->except(['create', 'edit']);
    Route::apiResource('product', 'ProductController')->except(['create', 'edit']);
    Route::apiResource('filter', 'FilterController')->except(['create', 'edit']);
    Route::apiResource('filterlist', 'FilterListController')->except(['create', 'edit']);
    Route::apiResource('product-type', 'ProductTypeController')->except(['create', 'edit']);

    Route::post('user/update', 'UserController@update');
});

Route::prefix('user')->group(function () {
    Route::post('register', 'UserController@register');
    Route::post('activate', 'UserController@activate');
    Route::post('login', 'UserController@login');
});